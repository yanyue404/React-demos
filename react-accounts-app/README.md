# react-accounts-app
> 小型财务系统

## Build Setup

````bash
# install dependencies
npm install

# serve with hot reload at http://localhost:3000/
yarn start

# build for production with minification
yarn build
````

## Useful links

- [mockapi](https://www.mockapi.io/projects)
- [json-server](https://github.com/typicode/json-server)
- [create-react-app](https://github.com/facebook/create-react-app)
- [reactjs.org](https://reactjs.org/docs/hello-world.html)


#### 参考链接
- https://www.rails365.net/playlists
