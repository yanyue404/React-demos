# React-demos [![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](LICENSE)
> 我的one！ [2017 react生态圈](https://risingstars.js.org/2017/zh/#section-react)

## Flag

- react-yanxuan
- react-blog

## Guide

### react

- [官方文档](https://reactjs.org/) / [中文文档](http://www.css88.com/react/)
- [React 技术栈系列教程](http://www.ruanyifeng.com/blog/2016/09/react-technology-stack.html) , by [ruanyifeng](https://github.com/ruanyf)
- [react-bits-CN-中文版](https://github.com/hateonion/react-bits-CN) / [react-bits-英文原版](https://github.com/vasanthk/react-bits)

### react-router

- [React Router 中文文档](http://react-guide.github.io/react-router-cn/)

## Tutorial

- [rails365.net](https://www.rails365.net/playlists)
- [React-Study](https://github.com/minooo/React-Study) ，by [minnooo](https://github.com/minooo)
- [12 步 30 分钟，完成用户管理的 CURD 应用 (react+dva+antd)](https://github.com/sorrycc/blog/issues/18)

## 编码规范

- [英文原版](https://github.com/airbnb/javascript/tree/master/react) / [中文版](https://github.com/JasonBoy/javascript/tree/master/react)
- [react-cookbook](https://github.com/shimohq/react-cookbook) - 编写简洁漂亮，可维护的 React 应用
## Rep

- [react-ssr](https://github.com/minooo/react-ssr) - react16 next.js4 antd-mobile2 redux
- [React-Study](https://github.com/minooo/React-Study) - 渐进式学习React生态圈
- [react](https://github.com/duxianwei520/react) - 一个react+redux+webpack+ES6+antd的SPA的后台管
理底层框架

## UI

- [ant-design](https://github.com/ant-design/ant-design) -ant A UI Design Language http://ant.design
- [material-ui](https://github.com/mui-org/material-ui) - React components that implement Google's Material Design. http://www.material-ui.com
- [ant-design-pro](https://github.com/ant-design/ant-design-pro) -  An out-of-box UI solution for enterprise applications http://pro.ant.design/
- [ant-design-mobile](https://github.com/ant-design/ant-design-mobile) - 一个基于 Preact / React / React Native 的 UI 组件库
- [react-bootstrap](https://github.com/react-bootstrap/react-bootstrap)

## Demos

- [react-accounts-app](https://github.com/xiaoyueyue165/react-accounts-app) - 小型财务系统
- [todomvc-demos](https://github.com/xiaoyueyue165/todomvc-demos) - angular1.x，vue，react 

## Build

- [create-react-app](https://github.com/facebook/create-react-app)
- [react-babel-webpack 项目样板](https://github.com/ruanyf/react-babel-webpack-boilerplate) , by [ruanyifeng](https://github.com/ruanyf)
- [dva-基于 redux、redux-saga 和 react-router 的轻量级前端框架](https://github.com/dvajs/dva)

#### Useful Links

- [分享关于React组件规范化的一些建议](https://github.com/minooo/React-Study/issues/6) by [minooo](https://github.com/minooo)



 

